# CodeBox : A Simple Vagrant Box

I usually use this as a starting point when building vagrant boxes for headless tests. 
However, it is also nice for running simple command line code projects as well in c, c++, nodejs etc.



## What's in the box:

- ack
- git
- Vim
- Spf13-VimPlugin (includes vundle, themes, & more)
- nodejs
- npm
- zsh (gets set as default shell)
- oh-my-zsh
- Xvfb
- Google-Chrome-Stable
- tree

## Getting Started
---

### First things first

Required Tools: [vagrant](http://docs.vagrantup.com/v2/installation/), [ansible](http://docs.ansible.com/intro_installation.html), & [virtualbox](https://www.virtualbox.org/wiki/Downloads).

### Workspace setup

First you'll probably want to detach this project from git once you download it...

```bash
sudo rm -r ./.git
```

_This prevents annoyance later on when creating new repos in the workspace folder within the vm._ 

Now create the workspace directory...

```bash
mkdir workspace
```

_This folder will be shared between the vm and host machine.  
once you log into the box via ssh you can `cd` into workspace from `~/`._

## Using the Box
---

### Boot it

```bash
vagrant up
```

### Shut it down

```bash
vagrant halt
```

### Restart provisioning

```bash
vagrant provision
```

### SSH into it

```bash
vagrant ssh
```

### Kill it (loss of data can occur)

```bash
vagrant destroy
```

